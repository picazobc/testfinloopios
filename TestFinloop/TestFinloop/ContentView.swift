//
//  ContentView.swift
//  TestFinloop
//
//  Created by Julio Picazo on 15/07/20.
//  Copyright © 2020 TestFinloop. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
