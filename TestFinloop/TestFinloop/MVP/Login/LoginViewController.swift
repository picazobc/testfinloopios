//
//  LoginViewController.swift
//
//  Created by Julio Picazo on 19/06/20.
//  Copyright © 2017 TestFinloop. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    //MARK: - Properties
    @IBOutlet weak var userInput: UITextField!
    @IBOutlet weak var passInput: UITextField!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var passView: UIView!
    
    var presenter: LoginViewPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter =  LoginViewPresenter()
        presenter.delegate = self
        validateLogin()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.userInput.text = ""
        self.passInput.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func login(_ sender: Any) {
        self.presenter.loginUser(user: userInput.text ?? "", password: passInput.text ?? "")
    }
    
    func validateLogin() {
        self.presenter.validatSession()
    }
}

extension LoginViewController: LoginPresenterDelegate {
    func showAlertMessage(title: String, message: String) {
        Popup.Alert(selfVC: self, delegate: nil, title: title, message: message)
    }
    
    func goToList() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let listView = mainStoryboard.instantiateViewController(withIdentifier: "NavigationFeedController")
        listView.modalPresentationStyle =  .overCurrentContext
        self.navigationController?.present(listView, animated: false, completion: nil)
    }
}
