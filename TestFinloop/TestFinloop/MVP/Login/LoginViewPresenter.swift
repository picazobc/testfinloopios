//
//  LoginViewPresenter.swift
//  TestFinloop
//
//  Created by Julio Picazo on 15/07/20.
//  Copyright © 2020 TestFinloop. All rights reserved.
//

import Foundation
import UIKit

protocol LoginPresenterDelegate {
    func showAlertMessage(title: String,message: String)
    func goToList()
}

class LoginViewPresenter  {
    var delegate: LoginPresenterDelegate!
    func loginUser(user: String, password: String) {
        if user.isEmpty  && password.isEmpty {
            self.delegate.showAlertMessage(title:"", message:"Llena los campos para continuar")
        } else {
            validateUserData(user: user, password: password)
        }
    }
    
    func validatSession() {
        let defaults = UserDefaults.standard
        if let jwtString = defaults.value(forKey: "jwt") as? String{
            if jwtString != ""{
                self.delegate.goToList()
            }
        }
    }
    
    func validateUserData(user: String, password: String) {
        if InternetConection.isInternetAvailable() {
            let headers:[String:String] = ["accept": "application/json"]
            let parameters:[String:Any] = ["email": user,
                                           "password":password]
            let requestObj = RequestObject(url: StaticObjects.Urls.login, method: .post, headers: headers, parameters: parameters)
            RequestAlamofire.requestMundial(requestObject: requestObj, completion: { (success, error, jsonResult, isLogout) in
                if success{
                    if jsonResult != nil{
                        let defaults = UserDefaults.standard
                        if let idString = jsonResult?["id"] as? String{
                            defaults.set(idString, forKey: "idUser")
                        }
                      
                        if let jwtString = jsonResult?["jwt"] as? String{
                            defaults.set(jwtString, forKey: "jwt")
                        }
                        self.delegate.goToList()
                    }
                }else{
                    self.delegate.showAlertMessage(title: "", message: error ?? "")
                }
            })
        }else {
            self.delegate.showAlertMessage(title: "", message: StaticObjects.Messages.messageNoInternet)
        }
    }
    
}
