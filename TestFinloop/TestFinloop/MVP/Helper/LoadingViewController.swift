//
//  File.swift
//  TestFinloop
//
//  Created by Julio Picazo on 19/06/20.
//  Copyright © 2020 TestFinloop. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
    
    //MARK: - Properties06
    @IBOutlet weak var loadingImageView: UIImageView!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Loop de imagenes
        var images: [UIImage] = []
        for i in 1...40 {
            if let image = UIImage(named: "\(i)"){
                images.append(image)
            }
        }
        
        loadingImageView.animationImages = images
        loadingImageView.animationDuration = 2.0
        
        self.loadingImageView.alpha = 1
        self.loadingImageView.startAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
