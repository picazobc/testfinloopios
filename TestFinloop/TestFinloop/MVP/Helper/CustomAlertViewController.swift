//
//  File.swift
//  TestFinloop
//
//  Created by Julio Picazo on 19/06/20.
//  Copyright © 2020 TestFinloop. All rights reserved.
//
import UIKit

//Delegate
protocol AlertDelegate{
    func closeAlert()
}

class CustomAlertViewController: UIViewController {
    
    //MARK: - Properties
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Variables
    var delegateCustom: AlertDelegate?
    var messageString:String = ""
    var titleString:String = ""
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Settings
        self.titleLabel.text = self.titleString
        self.messageLabel.text = self.messageString
        
        //Show popup
        self.showAnimate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark: - Actions
    @IBAction func closeAlert(_ sender: UIButton) {
        self.removeAnimate()
    }
    
    //MARK: - Animations
    func showAnimate(){
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        self.navigationController?.navigationBar.alpha = 0.4
        self.view.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
        self.view.alpha = 0.0
        
        view.endEditing(true)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        
        UIView.animate(withDuration: 0.25, animations: {
            
            self.navigationController?.navigationBar.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
            self.view.alpha = 0.0
            
        }, completion:{(finished : Bool)  in
            if (finished){
                if self.delegateCustom != nil{
                    self.delegateCustom!.closeAlert()
                }
                
                self.view.removeFromSuperview()
                self.navigationController?.navigationBar.isUserInteractionEnabled = true
            }
        })
    }
    
}
