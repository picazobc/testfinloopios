

import UIKit
import SwiftyJSON
import Alamofire
import SystemConfiguration


//MARK: Static elements
open class StaticObjects{
    
    struct Urls {
        
        static let login = backendURL + "/login"
        static let getUsuarios = backendURL + "/users"

    }
    
   
    struct Messages {
        //Error general
        static let messageErrorString = "Algo salió mal, por favor intenta de nuevo más tarde."
        static let messageNoInternet = "Por favor, verifica tu conexión a Internet."
    }
}
class Popup{
    class func Alert(selfVC: UIViewController, delegate:AlertDelegate?, title:String, message:String){
        let popOverVC = selfVC.storyboard!.instantiateViewController(withIdentifier: "CustomAlertPopup") as! CustomAlertViewController
        
        //Init
        popOverVC.messageString = message
        popOverVC.titleString = title
        popOverVC.delegateCustom = delegate
        
        //Add
        selfVC.addChild(popOverVC)
        popOverVC.view.frame = UIScreen.main.bounds
        selfVC.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: selfVC)
    }
    
}

class Loading{
    class func AddView(selfVC: UIViewController){
        let loadingVC = selfVC.storyboard!.instantiateViewController(withIdentifier: "LoadingViewController") as! LoadingViewController
        loadingVC.view.alpha = 0
        //Add
        selfVC.addChild(loadingVC)
        loadingVC.view.frame = UIScreen.main.bounds
        selfVC.view.addSubview(loadingVC.view)
        UIView.animate(withDuration: 0.5) {
            loadingVC.view.alpha = 1
        }
        loadingVC.didMove(toParent: selfVC)
    }
    
    class func removeView(selfVC: UIViewController) {
        for view in selfVC.view.subviews{
            if let viewWithTag = view.viewWithTag(143) {
                UIView.animate(withDuration: 0.5, animations: {
                    viewWithTag.alpha = 0
                }, completion: { (success) in
                    viewWithTag.removeFromSuperview()
                })
            }
        }
    }
}
    
class RequestObject {
    var url: String
    var method: HTTPMethod
    var headers: [String:String]?
    var parameters: [String: Any]?
    
    init(url: String, method: HTTPMethod, headers: [String:String]?, parameters: [String: Any]?) {
        self.url = url
        self.method = method
        
        if headers == nil{
            self.headers = nil
        }else{
            self.headers = headers!
        }
        
        if parameters == nil{
            self.parameters = nil
        }else{
            self.parameters = parameters
        }
    }
}

open class RequestAlamofire {
    
    class func requestMundial(requestObject: RequestObject, completion: @escaping (_ succes: Bool, _ error: String?, _ jsonObject: NSDictionary?, _ isLogout:Bool) -> Void){
        Alamofire.request(requestObject.url, method: requestObject.method, parameters: requestObject.parameters, encoding: JSONEncoding.default, headers: requestObject.headers).responseJSON { (response:DataResponse<Any>) in
            
            //            print("STATUS CODE: \(String(describing: response.response?.statusCode))")
            
            switch(response.result) {
            case .success(_):
                
                if response.result.value != nil{
                    
                    if response.response?.statusCode == 200 || response.response?.statusCode == 204{
                        
                        let json = response.result.value as? NSDictionary
                        
                        DispatchQueue.main.async {
                            completion( true, nil, json, false)
                        }
                    }else if response.response?.statusCode == 400{
                        
                        var message = StaticObjects.Messages.messageErrorString
                        let json = response.result.value as? [String: Any]
                        if json != nil{
                            if let messageResult = json!["mensaje"] as? String{
                                message = messageResult
                            }
                        }
                        
                        DispatchQueue.main.async {
                            completion(false,message, nil, false)
                        }
                        
                    }else if response.response?.statusCode == 401{
                        
                        var message = StaticObjects.Messages.messageErrorString
                        let json = response.result.value as? [String: Any]
                        if json != nil{
                            if let messageResult = json!["mensaje"] as? String{
                                message = messageResult
                            }
                        }
                        
                        DispatchQueue.main.async {
                            completion(false,message, nil, true)
                        }
                        
                    }else{
                        print("Status code: \(String(describing: response.response?.statusCode))")
                        print(response.description)
                        let message = StaticObjects.Messages.messageErrorString
                        
                        DispatchQueue.main.async {
                            completion(false,message, nil, false)
                        }
                    }
                    
                }else{
                    
                    let message = StaticObjects.Messages.messageErrorString
                    
                    DispatchQueue.main.async {
                        completion(false,message, nil, false)
                    }
                }
                
                break
                
            case .failure(_):
                
                let message = StaticObjects.Messages.messageErrorString
                
                DispatchQueue.main.async {
                    completion(false,message, nil, false)
                }
                
                break
            }
        }
    }
}

open class InternetConection{
    class func isInternetAvailable() -> Bool{
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}
