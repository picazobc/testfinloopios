//
//  RegisterModel.swift
//  TestFinloop
//
//  Created by Julio Picazo on 15/07/20.
//  Copyright © 2020 TestFinloop. All rights reserved.
//

import Foundation

struct RegisterModel {
    var name: String?
    var password: String?
    var email: String?
}
