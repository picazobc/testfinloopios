//
//  RegisterViewController.swift
//  TestFinloop
//
//  Created by Julio Picazo on 19/06/20.
//  Copyright © 2020 Julio Picazo. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var emailInput: UITextField!
    @IBOutlet weak var passInput: UITextField!
    var presenter: RegisterViewPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = RegisterViewPresenter()
        presenter.delegate = self
    }
    @IBAction func register(_ sender: Any) {
        view.endEditing(true)
        self.presenter.registerUser(name: nameInput.text ?? "", email: emailInput.text ?? "", password:  passInput.text ?? "")
    }
}

extension RegisterViewController: RegisterViewDelegate {
    func showAlertMessage(title: String, message: String) {
        Popup.Alert(selfVC: self, delegate: nil, title: title, message: message)
    }
        
    func addLoading() {
        Loading.AddView(selfVC: self)
    }
    
    func removeLoading() {
        Loading.removeView(selfVC: self)
    }
    
    func goToList() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let listView = mainStoryboard.instantiateViewController(withIdentifier: "NavigationFeedController")
        listView.modalPresentationStyle =  .overCurrentContext
        self.navigationController?.present(listView, animated: false, completion: nil)
    }
}
