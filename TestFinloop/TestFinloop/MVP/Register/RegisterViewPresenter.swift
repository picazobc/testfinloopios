//
//  RegisterViewPresenter.swift
//  TestFinloop
//
//  Created by Julio Picazo on 15/07/20.
//  Copyright © 2020 TestFinloop. All rights reserved.
//

import Foundation

protocol RegisterViewDelegate {
    func showAlertMessage(title: String,message: String)
    func goToList()
}

class RegisterViewPresenter  {
    var delegate: RegisterViewDelegate!
    
    func registerUser(name: String, email: String, password: String) {
        if name.isEmpty && password.isEmpty &&  email.isEmpty {
            self.delegate.showAlertMessage(title: "",
                                           message: "Llena los campos para continuar")
        } else {
            if InternetConection.isInternetAvailable() {
                let headers:[String:String] = ["accept": "application/json"]

                let parameters:[String:Any] = ["username": name,
                                              "password": password,
                                              "email": email]
               
                let requestObj = RequestObject(url: StaticObjects.Urls.getUsuarios, method: .post, headers: headers, parameters: parameters)
               
                RequestAlamofire.requestMundial(requestObject: requestObj, completion: { (success, error, jsonResult, isLogout) in
                    if success {
                        if jsonResult != nil{
                            let defaults = UserDefaults.standard

                            if let idString = jsonResult?["id"] as? String{
                               defaults.set(idString, forKey: "idUser")
                            }

                            if let jwtString = jsonResult?["jwt"] as? String{
                               defaults.set(jwtString, forKey: "jwt")
                            }
                            self.delegate.goToList()
                       }
                   }else{
                        self.delegate.showAlertMessage(title: "", message: error ?? "")
                   }
               })
               
           }else {
                self.delegate.showAlertMessage(title: "", message: StaticObjects.Messages.messageNoInternet)
           }
        }
    }
}
