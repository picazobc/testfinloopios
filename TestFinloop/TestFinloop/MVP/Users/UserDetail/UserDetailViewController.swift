//
//  UserDetailViewController.swift
//  TestFinloop
//
//  Created by Julio Picazo on 19/06/20.
//  Copyright © 2020 TestFinloop. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {
    
    var currentUser: UserModel?

    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblUsername.text = currentUser?.name
        self.lblEmail.text =  currentUser?.email
        // Do any additional setup after loading the view.
    }
}
