//
//  UserTableViewPresenter.swift
//  TestFinloop
//
//  Created by Julio Picazo on 15/07/20.
//  Copyright © 2020 Julio Picazo. All rights reserved.
//

import Foundation


protocol UserTableViewDelegate {
    func showAlertMessage(title: String,message: String)
    func reloadTable()
}

class UserTableViewPresenter {
    var usuariosList:[UserModel] = []
    var currentUser:[UserModel] = []

    var delegate: UserTableViewDelegate!
    private var defaults = UserDefaults.standard

    func getInfoUsers() {
        if InternetConection.isInternetAvailable(){
            if let token = defaults.value(forKey: "jwt") as? String{
                let headers:[String:String] = ["authorization": "Bearer: "+token,
                                               "accept": "application/json"]
                
                let requestObj = RequestObject(url: StaticObjects.Urls.getUsuarios, method: .get, headers: headers, parameters: nil)
                print("URL: \(StaticObjects.Urls.getUsuarios)")
                print("HEADERS: \(headers)")
                RequestAlamofire.requestMundial(requestObject: requestObj, completion: { (success, error, jsonResult, isLogout) in
                    if success{
                        if jsonResult != nil{
                            if let usuarios = jsonResult!["users"] as? [NSDictionary]{
                                var usuariosList:[UserModel] = []
                                for usuario in usuarios{
                                    if let _id = usuario["_id"] as? String, let id = usuario["id"] as? String, let username = usuario["username"] as? String, let email = usuario["email"] as? String{
                                        let userLocal = UserModel.init(_id: _id, id: id, name: username,email: email)
                                        usuariosList.append(userLocal)
                                    }
                                }
                                self.usuariosList = usuariosList
                                self.delegate.reloadTable()
                            }
                        }
                    }else{
                        self.delegate.showAlertMessage(title: "", message: error ?? "")
                    }
                })
            }
        }else{
            self.delegate.showAlertMessage(title: "", message: StaticObjects.Messages.messageNoInternet)
        }
    }
    
    func getUserCount() ->Int {
        return usuariosList.count
    }
    
    func getuserList() -> [UserModel] {
        return usuariosList
    }
    
    func getUserList(index: Int) -> UserModel {
        return usuariosList[index]
    }
}
