//
//  UserTableViewController.swift
//  TestFinloop
//
//  Created by Julio Picazo on 19/06/20.
//  Copyright © 2020 TestFinloop. All rights reserved.
//

import UIKit

class UserTableViewController: UITableViewController {
    var presenter: UserTableViewPresenter!
    let cellIdentifier = "userCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter =  UserTableViewPresenter()
        presenter.delegate = self
        presenter.getInfoUsers()
    }
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.getUserCount()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @IBAction func logoutAction(_ sender: UIButton) {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let listView = mainStoryboard.instantiateViewController(withIdentifier: "InitNavigationController")
        listView.modalPresentationStyle =  .overCurrentContext
        self.present(listView, animated: false, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! UserTableViewCell
        let usuarioCurrent = self.presenter.getUserList(index: indexPath.row)
        cell.userNameLabel.text = usuarioCurrent.name
        cell.emailLabel.text = usuarioCurrent.email
        return cell
    }
    //MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailUser"{
            if let destinationVC = segue.destination as? UserDetailViewController {
                let cell = sender as? UserTableViewCell
                let indexPath = tableView.indexPath(for: cell!)
                destinationVC.currentUser = self.presenter.getUserList(index: indexPath?.row ?? -1)
            }
        }
    }
}

extension UserTableViewController: UserTableViewDelegate {
    func showAlertMessage(title: String, message: String) {
        Popup.Alert(selfVC: self, delegate: nil, title: title, message: message)
    }
    
    func reloadTable() {
        self.tableView.reloadData()
    }
}
