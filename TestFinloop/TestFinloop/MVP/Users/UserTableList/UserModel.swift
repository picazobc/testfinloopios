//
//  User.swift
//  TestFinloop
//
//  Created by Julio Picazo on 15/07/20.
//  Copyright © 2020 Julio Picazo. All rights reserved.
//

import Foundation

struct UserModel {
    
    let _id:String
    let id:String
    let name:String
    let email:String
    
    
    init(_id:String, id:String, name:String, email:String) {
        self._id = _id
        self.id = id
        self.name = name
        self.email = email
    }
}
    


